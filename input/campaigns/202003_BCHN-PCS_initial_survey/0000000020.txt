Section A - What you wish to see in Bitcoin Cash Node (BCHN) full node project
------------------------------------------------------------------------------

1. Dedication to the success of the currency (not the node implementation)
2. Transparency
3. Principled, practical and tolerant leadership
4. Continuous improvement in response to user feedback
5. Continuous improvement in performance

Section B - What you wish to see in Bitcoin Cash (BCH) overall
--------------------------------------------------------------

1. Adoption as money in all forms (MOE / SOV / UOA)
2. Forking changes require 75% miner support for an exact specification+implementation+activation. No flag-day or flag-block forks -- they are insensible to miner support.

Section C - Information about yourself
--------------------------------------

- [X] - a BCH user
- [X] - a Bitcoin Cash protocol or full node client developer
- [X] - a person with significant amounts of their long term asset holdings in BCH
