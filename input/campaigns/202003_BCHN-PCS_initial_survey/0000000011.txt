Section A - What you wish to see in Bitcoin Cash Node (BCHN) full node project
------------------------------------------------------------------------------

1. Fix the DAA. It is horribly broken. This needs to be addressed ASAP.
2. Work directly with miners on important features to ensure we reverse the trend of complete centralization of BCH hashrate (whether actual hardware or via pool) into only 1 or 2 entities.
3. Clear and open communication to the community about BCHN plans. Realistic approach to needed vs. wish-list features.


Section B - What you wish to see in Bitcoin Cash (BCH) overall
--------------------------------------------------------------

1. Seriously push and foster adoption of BCH as peer-to-peer electronic cash.
2. P2P BCH trading/exchanging is a real need that needs to be addressed. 
3. Streamline communications. Maybe a semi-private/moderated (to avoid trolls/shills and keep on topic) place for (verified) protocol/node devs, app developers, miners, pool operators, and others that are invested into the long-term success of BCH to discuss solutions to problems (or new features) and be on the same page, as much as is possible, prior to using open and heavily astroturfed places like r/btc.  


Section C - Information about yourself
--------------------------------------

You would describe yourself as (you can check multiple responses):

- [x] - a Bitcoin Cash miner or mining executive
- [x] - a Bitcoin Cash pool operator or executive
- [ ] - a Bitcoin Cash exchange operator or executive
- [ ] - a Bitcoin Cash payment service provider operator or executive
- [ ] - a Bitcoin Cash protocol or full node client developer
- [ ] - a Bitcoin Cash layered application developer (SPV wallets, token systems etc)
- [ ] - an executive of a retail business using Bitcoin Cash
- [x] - a person with significant amounts of their long term asset holdings in BCH
- [ ] - someone who provides liquidity, derivative and other financial services in Bitcoin Cash
- [x] - a BCH user
- [x] - other - please specify: director of a company that specializes in management and monitoring software for SHA256 miners.
