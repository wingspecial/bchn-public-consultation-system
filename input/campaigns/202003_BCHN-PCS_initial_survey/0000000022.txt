Section A - What you wish to see in Bitcoin Cash Node (BCHN) full node project
------------------------------------------------------------------------------

1. Generally: Scaling the node software improvements on similar level as Bitcoin Unlimited and Flowee have done. 
2. Work towards efficient gigabyte blocks that actually propagate well.
3. True multicore processing of all incoming and outgoing data. With AMD processors (EPYC, RYZEN) and M2.SSDs this will enable for practically unlimited scaling.
4. Store UXTO database on a separate drive(think M2.SSD) 

Section B - What you wish to see in Bitcoin Cash (BCH) overall
--------------------------------------------------------------

1. Gigabyte blocks enabled, VISA + Mastercard + Paypal combined throughput.
2. Peer2Peer cash for every person on earth most importantly, before all else.
3. Privacy/Fungibility (probably through CashFusion and similar)
4. Promoting and working towards of non-cash uses(smart contracts, blockchain-virtual-machine, SLP tokens, communication services, other) but only if they do not significantly impede, slow down works on the main function [Cash] or worsen the experience of users using Bitcoin Cash as money for the world.
5. An uncensorable moderation-optional decentralized communication platform would be useful.

Section C - Information about yourself
--------------------------------------

You would describe yourself as (you can check multiple responses):

- [ ] - a Bitcoin Cash miner or mining executive
- [ ] - a Bitcoin Cash pool operator or executive
- [ ] - a Bitcoin Cash exchange operator or executive
- [ ] - a Bitcoin Cash payment service provider operator or executive
- [x] - a Bitcoin Cash protocol or full node client developer
- [ ] - a Bitcoin Cash layered application developer (SPV wallets, token systems etc)
- [ ] - an executive of a retail business using Bitcoin Cash
- [x] - a person with significant amounts of their long term asset holdings in BCH
- [ ] - someone who provides liquidity, derivative and other financial services in Bitcoin Cash
- [x] - a BCH user
- [x] - other - please specify: ShadowOfHarbringer, 2010 Early Adopter, Shill Hunter, Bitcoin Cash developer.
