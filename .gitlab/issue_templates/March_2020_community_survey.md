March 2020 Survey Feedback
==========================

Section A - What you wish to see in Bitcoin Cash Node (BCHN) full node project
------------------------------------------------------------------------------

It can be anything: technical improvement, organizational, directional...
Please rank in order of importance to you (1 = highest priority)

1.
2.
3.
...
(add or delete items as you need)


Section B - What you wish to see in Bitcoin Cash (BCH) overall
--------------------------------------------------------------

Generally things that affect the whole protocol / currency / ecosystem,
and not BCHN specifically.
Please rank in order of importance to you (1 = highest priority)

1.
2.
3.
...
(add or delete items as you need)


Section C - Information about yourself
--------------------------------------
This will help us to better assess the input.
Please put an 'X' or and 'x' into whichever of the following applies:

You would describe yourself as (you can check multiple responses):

- [ ] - a Bitcoin Cash miner or mining executive
- [ ] - a Bitcoin Cash pool operator or executive
- [ ] - a Bitcoin Cash exchange operator or executive
- [ ] - a Bitcoin Cash payment service provider operator or executive
- [ ] - a Bitcoin Cash protocol or full node client developer
- [ ] - a Bitcoin Cash layered application developer (SPV wallets, token systems etc)
- [ ] - an executive of a retail business using Bitcoin Cash
- [ ] - a person with significant amounts of their long term asset holdings in BCH
- [ ] - someone who provides liquidity, derivative and other financial services in Bitcoin Cash
- [ ] - a BCH user
- [ ] - other - please specify: 
